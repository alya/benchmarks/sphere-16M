Flow over a sphere

* Mesh: 16.7M elements, 2.9M nodes
* Modules: Nastin
* Physics: turbulent flow over a sphere
* Numerical model: Vreman turbulence model, convective term using the EMACS scheme
* Solution strategy: fractional step with Runge-Kutta of order 3
* Algebraic solvers: CG
